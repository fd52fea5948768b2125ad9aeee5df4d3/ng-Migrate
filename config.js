module.exports = {
    MYSQL : {
        COORDINATOR_MAX_ID: 23
    },
    MONGO: {
        DB_ADDRESS : "mongodb://elabuser:elabuserdbpassword@localhost/elabdb",
        DB_NAME : "elabdb",
    },
    TO_COPY: {
        FLAGS_TABLE: false,
        COURSE_REQ_TABLE: false,
        SESSION_TABLE : false,
        COORDINATOR_TABLE: false,
        COURSE_TABLE: false,
        USERS_TABLE: false,
        FACULTY_TABLES: false,
        STUDENT_TABLES: true
    },
    VERBOUS: false
}