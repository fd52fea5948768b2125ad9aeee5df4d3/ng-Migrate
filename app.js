const colors = require('colors');
const config = require('./config');
const Promise = require('promise');
const courses = require('./courses');
const  mysql = require('mysql2/promise');
const MongoClient = require('mongodb').MongoClient;

// Modules
const flagModule = require('./core/flags');
const courseReqModule = require('./core/course-req');
const usersModule = require('./core/users');
const sessionModule = require('./core/session');
const coordinatorModule = require('./core/coordinator');
const questionModule = require('./core/question');
const facultyModule = require('./core/faculty');
const studentsModule = require('./core/students');

(async () => { try {
    //Staring App 
    console.log('Starting Migrator...\n');
    
    //Mongo Connection
    console.log('Connecting to Mongo DB..'.yellow);
    const client = await MongoClient.connect(config.MONGO.DB_ADDRESS);
    console.log("Connected to MongoDB!\n".green);
    const db = client.db(config.MONGO.DB_NAME);

    // MySQL Connection
    console.log('Connecting to MySQL..'.yellow);
    const connection = await mysql.createConnection({host:'192.168.30.129', user: 'elabuser',password: 'elabuserdbpassword', database: 'elab_db'});
               //await connection.connect();
    console.log("Connected to SQL!\n".green);
    
    // Migrate Table
    if(config.TO_COPY.FLAGS_TABLE) {
        console.log("Migrating Flags Table..".yellow);
        await flagModule(db,connection);
    }
    
    if(config.TO_COPY.COURSE_REQ_TABLE) {
        console.log("Migrating Course Req. Table..".yellow);
        await courseReqModule(db,connection);
    }
    
    if(config.TO_COPY.SESSION_TABLE) {
        console.log("Migrating Session Table..".yellow);
        await sessionModule(db,connection);
    }

    if(config.TO_COPY.COORDINATOR_TABLE) {
        console.log("Migrating Session Table..".yellow);
        for(let i=11;i<=config.MYSQL.COORDINATOR_MAX_ID;i++) {
            console.log(`Cooridnator ${i}`.yellow);
            await coordinatorModule(db,connection,i);
        }
    }

    if(config.TO_COPY.COURSE_TABLE) {
        console.log("Migrating Course Table..".yellow);
        for(let i=11;i<=config.MYSQL.COORDINATOR_MAX_ID;i++) {
            console.log(`${i} Question Table`.yellow);
            await questionModule(db,connection,courses[i].toLowerCase());
        }
    }

    if(config.TO_COPY.USERS_TABLE) {
        console.log("Migrating Users Table..".yellow);
        await usersModule(db,connection);
    }

    if(config.TO_COPY.FACULTY_TABLES) {
        console.log("Migrating Faculty Table..".yellow);
        let t = (new Date()).getTime();
        await facultyModule(db,connection,t);
    }

    if(config.TO_COPY.STUDENT_TABLES) {
        console.log("Migrating Students Table..".yellow);
        await studentsModule(db,connection);
    }
    
    //Ending Both Connection
    client.close();
    console.log('Connection Closed to MongoDB'.underline);
    connection.end();
    console.log('Conection Closed to MySQL'.underline);    
}catch(error) {
    console.log(error);
}
})();
