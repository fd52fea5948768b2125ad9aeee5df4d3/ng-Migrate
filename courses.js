module.exports = {
    11 : "C",
    12 : "CPP",
    13 : "JAVA",
    14 : "DATA-STRUCTURE",
    15 : "OPERATING-SYSTEMS",
    16 : "DAA",
    17 : "MATHSLAB",
    18 : "PYTHON",
    19 : "PERL",
    20 : "R",
    21 : "CSHARP",
    22 : "HASKELL",
    23 : "RUBY"
}