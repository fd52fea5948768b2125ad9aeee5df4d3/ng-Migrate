var USERS_TABLE = 'users_table',
    USERS_REQ_TABLE = 'users_req_table',
    TOP_100_TABLE = "top100_table",
    SESSION_TABLE = "session_table",
    RANK_TABLE = "rank_table",
    FREQUENCY_TABLE = "frequency_table",
    FLAGS_TABLE = "flags",
    COURSE_TABLE = "course_table",
    LOGIN_TABLE = "login_table",
    PROGRESS_TABLE = "progress_table",
    COURSE_REQ_TABLE = "course_req_table";

module.exports = {
    USERS_TABLE : USERS_TABLE ,
    USERS_REQ_TABLE : USERS_REQ_TABLE,
    TOP_100_TABLE : TOP_100_TABLE ,
    SESSION_TABLE : SESSION_TABLE,
    RANK_TABLE : RANK_TABLE,
    FREQUENCY_TABLE : FREQUENCY_TABLE,
    FLAGS_TABLE : FLAGS_TABLE,
    COURSE_TABLE : COURSE_TABLE,
    COURSE_REQ_TABLE : COURSE_REQ_TABLE,
    LOGIN_TABLE : LOGIN_TABLE,
    PROGRESS_TABLE : PROGRESS_TABLE,

    TABLES : [
        USERS_TABLE,
        PROGRESS_TABLE,
        USERS_REQ_TABLE,
        TOP_100_TABLE,
        SESSION_TABLE,
        RANK_TABLE,
        FREQUENCY_TABLE,
        FLAGS_TABLE,
        LOGIN_TABLE,
        COURSE_TABLE,
        COURSE_REQ_TABLE
    ],

    COORDINATOR_TABLE_SUFFIX : 'coordinator_',
    QUESTION_TABLE_PREFIX : '_question_table',
    STUDENT_TABLE_PREFIX : "std_db_",
    FACULTY_TABLE_PREFIX : "faculty_"
}
