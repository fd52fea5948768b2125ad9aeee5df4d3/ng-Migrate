const colors = require('colors');
const config = require('./../config');
const tables = require('./../tables');
const Promise = require('promise');

module.exports = function(db,connection) {
    return new Promise((resolve,reject) => {
        connection.query('SELECT * FROM `session_table`',function(err, results, fields) {
            if(err) {
                reject(err);
                return;
            }
            let collection = db.collection(tables.SESSION_TABLE);
            data = [];
            for(let i=11;i<=config.MYSQL.COORDINATOR_MAX_ID;i++) {
                data.push({_id: ("" + i), courseID: ("" + i), sessions : {}});
            }

            results.forEach(element => {
                let cid = parseInt(element.COURSE_SESSION_ID / 100);
                data[cid - 11].sessions[element.COURSE_SESSION_ID] = element.SESSION_NAME;
            });
            collection.insertMany(data).then(() => {
                resolve();
            },error => {
                reject(error);
            })
        });
    });
}


