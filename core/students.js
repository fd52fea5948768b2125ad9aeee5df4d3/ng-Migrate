const colors = require('colors');
const config = require('./../config');
const tables = require('./../tables');
const Promise = require('promise');

module.exports = function(db,connection) {
    return new Promise((resolve,reject) => {
        connection.query(`SELECT * FROM \`users_table\` WHERE ROLE = 'S'`,async function(err, results, fields) {
            if(err) {
                reject(err);
                return;
            }
            if(results.length == 0) {
                resolve();
                return;
            }
            let total = results.length;
            for(let i=0;i<total;i++) {
                if(config.VERBOUS)
                    console.log(`Migrating ${i+1} / ${total} => ` + results[i].USER_ID);
                await addFaculty(db,connection,results[i]);
            }
            resolve();

        });
    });
}

function addFaculty(db,connection,e) {
    return new Promise((resolve,reject) => {
        connection.query(`SELECT * FROM \`std_db_${e.USER_ID}\``,async function(err, results, fields) {
            if(err) {
                reject(err);
                return;
            }
            if(results.length == 0) {
                resolve();
                return;
            }
            data = [];
            let total = results.length;
            for(let i=0;i<total;i++) {
                let element = results[i];
                let sid = parseInt(element.SEQUENCE_ID);
                data.push({
                    _id: element.Q_ID,
                    questionID: element.Q_ID,
                    sequenceID: element.SEQUENCE_ID,
                    sessionID: "" + parseInt(sid/1000),
                    courseID: "" + parseInt(sid/100000),
                    status: element.STATUS,
                    code: element.CODE ? element.CODE : ''
                });
            }

            let collection = db.collection(tables.STUDENT_TABLE_PREFIX + e.USER_ID.toLowerCase());
            collection.insertMany(data).then(() => {
                resolve();
            },error => {
                reject(error);
            })
        });
    });
}

