const colors = require('colors');
const config = require('./../config');
const tables = require('./../tables');
const Promise = require('promise');

module.exports = (db,connection) => {
    return new Promise((resolve,reject) => {
        connection.query('SELECT * FROM `flags`',function(err, results, fields) {
            if(err) {
                reject(err);
                return;
            }
            let data = {
                _id: 'ADMIN_CONTROLS',
                canCopy: true,
                canFacultyRegister: true,
                canStudentRegister: true
            }

            results.forEach(element => {
                if(element.NAME == 'COPY_CONTROL') {
                    data.canCopy = element.VALUE === 1 ? false : true;
                }

                
                if(element.NAME == 'FACULTY_REGISTER') {
                    data.canFacultyRegister = element.VALUE === 1 ? false : true;
                }

                
                if(element.NAME == 'STUDENT_REGISTER') {
                    data.canStudentRegister = element.VALUE === 1 ? false : true;
                }
            });

            let collection = db.collection(tables.FLAGS_TABLE);
            collection.replaceOne({_id: data._id},data,{upsert: true}).then(() => {
                resolve();
            },(error) => {
                reject(error);
            });
        });
    });
}