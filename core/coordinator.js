const colors = require('colors');
const config = require('./../config');
const tables = require('./../tables');
const Promise = require('promise');

module.exports = function(db,connection,id) {
    return new Promise((resolve,reject) => {
        connection.query(`SELECT * FROM COORDINATOR_${id}`,function(err, results, fields) {
            if(err) {
                reject(err);
                return;
            }
            if(results.length < 1) {
                resolve();
                return;
            }
            let collection = db.collection(tables.COORDINATOR_TABLE_SUFFIX + id);
            data = [];
            results.forEach(element => {
                data.push({
                    _id: element.FACULTY_ID,
                    facultyID: element.FACULTY_ID,
                    name: element.FACULTY_NAME
                });
            });

            collection.insertMany(data).then(() => {
                resolve();
            },error => {
                reject(error);
            })
        });
    });
}


