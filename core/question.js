const colors = require('colors');
const config = require('./../config');
const tables = require('./../tables');
const Promise = require('promise');

module.exports = function(db,connection,id) {
    return new Promise((resolve,reject) => {
        connection.query(`SELECT * FROM \`${id}_question_table\` `,async function(err, results, fields) {
            if(err) {
                reject(err);
                return;
            }
            if(results.length < 1) {
                resolve();
                return;
            }
            let collection = db.collection(id + tables.QUESTION_TABLE_PREFIX);
            let total = results.length;
            for(let i=0;i<total;i++) {
                element = results[i];
                let qid = parseInt(element.Q_ID);
                if(config.VERBOUS)
                    console.log(`Question ${i+1} / ${total} => ${qid}`);


                let del = "\n###---###SEPERATOR---###---\n";
                let vTC = [];
                let hTC = [];
                let s = [];
                s = element.TESTCASE_1.split(del);
                if(s[0] != '0' && s[i] != '0') {
                    vTC.push({
                        input: s[0] == '0' ? "" : s[0],
                        output: s[1]
                    })
                }

                s = element.TESTCASE_2.split(del);
                if(s[0] != '0' && s[i] != '0') {
                    vTC.push({
                        input: s[0] == '0' ? "" : s[0],
                        output: s[1]
                    })
                }

                s = element.TESTCASE_3.split(del);
                if(s[0] != '0' && s[i] != '0') {
                    hTC.push({
                        input: s[0] == '0' ? "" : s[0],
                        output: s[1]
                    })
                }

                s = element.TESTCASE_4.split(del);
                if(s[0] != '0' && s[i] != '0') {
                    hTC.push({
                        input: s[0] == '0' ? "" : s[0],
                        output: s[1]
                    })
                }

                if(vTC.length == 0) {
                    console.log(`Error in QID : ${qid} . No Visible Test Case!!!`.red);
                }

                if(hTC.length == 0) {
                    if(config.VERBOUS)
                        console.log(`Error in QID : ${qid} . No Hidden Test Case!!!`.yellow);
                }
                

                data = {
                    _id: element.Q_ID,
                    questionName: element.Q_NAME,
                    sessionName: element.S_NAME,
                    sessionID: ("" + parseInt(qid / 1000000)),
                    courseID: ("" + parseInt(qid / 100000000)),
                    level: parseInt(qid/100000)%10,
                    questionDesc: element.Q_DESC,
                    questionID: qid,
                    visibleTestCases: vTC,
                    hiddenTestCases: hTC
                }
                await addToDB(collection,data);
            }
            resolve();
        });
    });
}

function addToDB(collection,data) {
    return new Promise((resolve,reject) => {
        collection.insertOne(data).then(() => {
            resolve();
        },error => {
            reject(error);
        })
    });
}



