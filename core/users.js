const colors = require('colors');
const config = require('./../config');
const tables = require('./../tables');
const Promise = require('promise');

module.exports = function(db,connection) {
    return new Promise((resolve,reject) => {
        connection.query('SELECT * FROM `users_table`',function(err, results, fields) {
            if(err) {
                reject(err);
                return;
            }
            let collection = db.collection(tables.USERS_TABLE);
            data = [];
            results.forEach(element => {
                data.push({
                    _id: element.USER_ID,
                    firstName: element.FIRST_NAME,
                    lastName: element.LAST_NAME,
                    mailID: element.MAIL_ID,
                    mobile: ("" + element.MOBILE),
                    password: element.PASSWORD,
                    role: element.ROLE,
                    dept: element.DEPT,
                    userID: element.USER_ID,
                    dob : (new Date(element.DOB)).getTime()
                });
            });

            collection.insertMany(data).then(() => {
                resolve();
            },error => {
                reject(error);
            })
        });
    });
}


