const colors = require('colors');
const config = require('./../config');
const tables = require('./../tables');
const Promise = require('promise');

module.exports = function(db,connection) {
    return new Promise((resolve,reject) => {
        connection.query('SELECT * FROM `course_reg_table`',function(err, results, fields) {
            if(err) {
                reject(err);
                return;
            }
            let collection = db.collection(tables.COURSE_REQ_TABLE);
            data = [];
            results.forEach(element => {
                data.push({
                    _id: element.REQ_ID,
                    courseID: ("" + element.COURSE_ID),
                    courseName: element.COURSE_NAME,
                    name: element.STUD_NAME,
                    regNo: element.STUD_ID,
                    status: parseInt(element.STATUS),
                    faculty: element.STAFF_ID
                });
            });

            collection.insertMany(data).then(() => {
                resolve();
            },error => {
                reject(error);
            })
        });
    });
}


