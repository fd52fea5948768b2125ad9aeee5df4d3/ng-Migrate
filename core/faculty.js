const colors = require('colors');
const config = require('./../config');
const tables = require('./../tables');
const Promise = require('promise');

module.exports = function(db,connection,time) {
    return new Promise((resolve,reject) => {
        connection.query(`SELECT * FROM \`users_table\` WHERE ROLE = 'F'`,async function(err, results, fields) {
            if(err) {
                reject(err);
                return;
            }
            if(results.length == 0) {
                resolve();
                return;
            }
            let total = results.length;
            for(let i=0;i<total;i++) {
                //if(config.VERBOUS)
                    console.log(`Migrating ${i+1} / ${total} => ` + results[i].USER_ID);
                await addFaculty(db,connection,results[i],time);
            }
            resolve();

        });
    });
}

function addFaculty(db,connection,e,t) {
    return new Promise((resolve,reject) => {
        connection.query(`SELECT * FROM \`faculty_${e.USER_ID}\``,async function(err, results, fields) {
            if(err) {
                reject(err);
                return;
            }
            if(results.length == 0) {
                resolve();
                return;
            }
            data = [];
            let total = results.length;
            for(let i=0;i<total;i++) {
                let element = results[i];
                data.push({
                    _id: element.STUD_ID + "" + element.COURSE_ID,
                    faculty: e.FIRST_NAME + " " + e.LAST_NAME,
                    facultyID: e.USER_ID,
                    regNo: element.STUD_ID,
                    courseID: element.COURSE_ID,
                    name: element.STUD_NAME,
                    courseName: element.COURSE_NAME,
                    level1: (element.LEVEL_1 + ""),
                    level2: (element.LEVEL_2 + ""),
                    level3: (element.LEVEL_3 + ""),
                    level1Updated: t,
                    level2Updated: t,
                    level3Updated: t
                });
            }

            let collection = db.collection(tables.PROGRESS_TABLE);
            collection.insertMany(data).then(() => {
                resolve();
            },error => {
                reject(error);
            })
        });
    });
}

